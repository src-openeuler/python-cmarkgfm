%global _empty_manifest_terminate_build 0
Name:		python-cmarkgfm
Version:	2024.1.14
Release:	1
Summary:	Minimal bindings to GitHub's fork of cmark
License:	MIT
URL:		https://github.com/theacodes/cmarkgfm
Source0:	https://files.pythonhosted.org/packages/7d/57/0480532cf04f77ff6f34985109079e87b9028444540731a5380abd0a3325/cmarkgfm-2024.1.14.tar.gz


%description
Bindings to GitHub's cmark Minimalist bindings to GitHub's fork of cmark.

%package -n python3-cmarkgfm
Summary:	Minimal bindings to GitHub's fork of cmark
Provides:	python-cmarkgfm = %{version}-%{release}
BuildRequires:	gcc
BuildRequires:	python3-devel
BuildRequires:	python3-cffi
BuildRequires:	python3-setuptools
BuildRequires:	python3-pytest

%description -n python3-cmarkgfm
Bindings to GitHub's cmark Minimalist bindings to GitHub's fork of cmark.

%package help
Summary:	Development documents and examples for cmarkgfm
Provides:	python3-cmarkgfm-doc

%description help
Bindings to GitHub's cmark Minimalist bindings to GitHub's fork of cmark.

%prep
%autosetup -n cmarkgfm-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-cmarkgfm -f filelist.lst
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Jul 12 2024 Hann <hannannan@kylinos.cn> - 2024.1.14-1
- Update package to version 2024.1.14-1
- Update cmark-gfm (0.29.0.gfm.6 -> 0.29.0.gfm.13)

* Fri Nov 11 2022 wangjunqi <wangjunqi@kylinos.cn> - 2022.10.27-1
- Update package to version 2022.10.27

* Sun Apr 24 2022 liweiganga <liweiganga@uniontech.com> 2022.3.4-1
- inital package

